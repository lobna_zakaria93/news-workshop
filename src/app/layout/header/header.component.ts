import { Component, EventEmitter, OnInit, Output } from "@angular/core";

@Component({
    selector:'app-header',
    templateUrl:'./header.component.html',
    styleUrls:['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    isSearch = false;
    opened = false;
    @Output()
    toggle: EventEmitter<boolean> = new EventEmitter<boolean>();
    constructor(){

    }

    ngOnInit(){

    }
    onShowSearchInput(){
        this.isSearch = true;
    }
    toggleMenu(){
        this.opened = !this.opened;
        this.toggle.emit(this.opened)
    }
}