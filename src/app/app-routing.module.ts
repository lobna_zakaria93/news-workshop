import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { NewsComponent } from './pages/news/news.component';
import { NewsDetailsComponent } from './pages/newsDetails/news-details.component';


const routes: Routes = [
  {
    path:'home',
    component:HomeComponent
  },
  {
    path:'news',
    component:NewsComponent
  },
  {
    path:'news/:id/details',
    component:NewsDetailsComponent
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'home',
    pathMatch: 'full'
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
