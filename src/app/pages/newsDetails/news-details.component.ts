import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { NewsService } from "src/app/services/news.service";

@Component({
    templateUrl:'news-details.component.html',
    styleUrls:['news-details.component.scss']
})
export class NewsDetailsComponent implements OnInit{
    newsId:any;
    IsWait = false;
    newsDetails:any;
    categoryList:any[] = [];
    value =50;
    relatedTopics = [
        {img:'./assets/images/img.png', title:'New Artificial Intelligence Apps'},
        {img:'./assets/images/img2.png', title:'Technology Development Seminar'},
        {img:'./assets/images/img3.png', title:'Next Step Of IOT[Internet Of Things]'},
    ]
    constructor(private router: ActivatedRoute,private newsService:NewsService) {
        this.newsId = this.router.snapshot.params.id;
     }

    ngOnInit(){
        this.getNews();
    }
    getNews(){
        this.IsWait = true;
        this.newsService.getJSON().subscribe(data => {
            data.articles.forEach((news:any) => {
                if(news.id == this.newsId){
                    this.newsDetails = news
                    this.IsWait = false;
                }
            });
            this.categoryList = data.sourceCategory;
           
        });
    }
}