import { Component, OnInit } from "@angular/core";
import { PageChangedEvent } from "ngx-bootstrap/pagination";
import { NewsService } from "src/app/services/news.service";

@Component({
    templateUrl:'news.component.html',
    styleUrls:['news.component.scss']
})
export class NewsComponent implements OnInit{
    page: number = 1;
    news:any[] = [];
    allNews: any[] = [];
    categories:any[] = [];
    IsWait = false;
    value =50;
    searchValue = '';
    constructor(private newsService:NewsService){}
    ngOnInit(){
        this.getNews();
    }

    getNews(){
        this.IsWait = true;
        this.newsService.getJSON().subscribe(data => {
            this.news=data.articles;
            this.allNews = data.articles;
            this.categories = data.sourceCategory;
            this.IsWait = false;
        });
    }
    pageChanged(event: PageChangedEvent): void {
        this.page = event.page;
    }
    sortByCategory(value:any){
        this.IsWait = true;
        if(value === 'all'){
            this.news = this.allNews;
            this.IsWait = false;
        } else {
            let arr:any[] = [];
            this.allNews.filter(s => {
                if(s.sourceID == value){
                    arr.push(s)
                }
            });
            this.news = arr;
            this.IsWait = false;
        }
        
    }
    onSearchClick(){
        if(this.searchValue == ''){
            this.news = this.allNews;
        } else {
            let x:any ;
            let arr:any[] = [];
            this.allNews.filter(data => {
                JSON.stringify(data).toLowerCase().includes(this.searchValue) ? arr.push(data) : x= false;
            });
            this.news = arr;
        }
        
    }
}