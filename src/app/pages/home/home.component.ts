import { Component, OnInit } from "@angular/core";
import { NewsService } from "src/app/services/news.service";

@Component({
    templateUrl:'home.component.html',
    styleUrls:['home.component.scss']
})
export class HomeComponent implements OnInit{
    news:any[] = [];
    carouselData = [
        {header:'first header' , paragragh:'is focused on inspiring the next generation of  kids Healthcare professionals.'},
        {header:'second header' , paragragh:'is focused on inspiring the next generation of  kids Healthcare professionals.'},
        {header:'third header' , paragragh:'is focused on inspiring the next generation of  kids Healthcare professionals.'}
    ];
    dataArray = [
        {class: 'fas fa-map-marker-alt' , text: 'Find Place'},
        {class: 'fas fa-eye' , text: 'A\'awen'},
        {class: 'fas fa-info-circle' , text: 'Omniyat'},
        {class: 'far fa-clock' , text: 'Give Time'},
        
    ]
    dataArray2 = [
        {class: 'far fa-grin-alt' , text: 'Tofoula'},
        {class: 'fas fa-mobile-alt' , text: 'Fundraising'},
        {class: 'fas fa-hand-holding-medical' , text: 'Zakat'},
    ]
    showIndicator = true;

    constructor(private newsService:NewsService){}
    ngOnInit(){
        this.getNews();
    }

    getNews(){
        this.newsService.getJSON().subscribe(data => {
            data.articles.forEach((item:any) => {
                if(item.showOnHomepage){
                   this.news.push(item)
                }
            });
        });
    }
}